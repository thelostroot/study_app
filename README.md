# study_app

> Обучающее приложение

## Build Setup

```bash
# Скачать и установить Node.js https://nodejs.org

# Перейти в каталог с репозиторием
cd C:\projects\study_app

# Установить все пакеты
npm install

# Запустить дев сервер(localhost:8080)
npm run dev
```
