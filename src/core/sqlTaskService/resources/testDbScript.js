export const createQueries = [
  `CREATE TABLE Categories
    (
        Id       INTEGER PRIMARY KEY CONSTRAINT PK_Categories,
        Name     NVARCHAR(100) NOT NULL
    )`,

  `CREATE TABLE Tags
    (
        Id       INTEGER PRIMARY KEY CONSTRAINT PK_Tags,
        Name     NVARCHAR(100) NOT NULL
    )`,

  `CREATE TABLE Users
    (
        Id        INTEGER PRIMARY KEY CONSTRAINT PK_Users,
        Confirmed BIT           NOT NULL,
        Email     NVARCHAR(450) NOT NULL,
        LastName  NVARCHAR(100) NOT NULL,
        Login     NVARCHAR(450) NOT NULL,
        Name      NVARCHAR(100) NOT NULL,
        Password  NVARCHAR(100) NOT NULL,
        Role      NVARCHAR(100)
    )`,

  `CREATE TABLE Posts
    (
        Id          INTEGER PRIMARY KEY CONSTRAINT PK_Posts,
        CategoryId  INT NOT NULL CONSTRAINT FK_Posts_Categories_CategoryId REFERENCES Categories,
        Image       NVARCHAR(100),
        PostText    NVARCHAR(100) NOT NULL,
        PublishTime DATETIME2     NOT NULL,
        Title       NVARCHAR(100) NOT NULL
    )`,

  `CREATE TABLE Comments
    (
        Id          INTEGER PRIMARY KEY  CONSTRAINT PK_Comments,
        PostId      INT           NOT NULL  CONSTRAINT FK_Comments_Posts_PostId  REFERENCES Posts,
        PublishTime DATETIME2     NOT NULL,
        Text        NVARCHAR(100) NOT NULL,
        UserId      INT           NOT NULL  CONSTRAINT FK_Comments_Users_UserId  REFERENCES Users
    )`,

  `CREATE TABLE PostTags
    (
        PostId INT NOT NULL  CONSTRAINT FK_PostTags_Posts_PostId  REFERENCES Posts,
        TagId  INT NOT NULL  CONSTRAINT FK_PostTags_Tags_TagId  REFERENCES Tags,
        CONSTRAINT PK_PostTags PRIMARY KEY (PostId, TagId)
    )`,

  `CREATE UNIQUE INDEX IX_Users_Email  ON Users (Email)`,
  `CREATE UNIQUE INDEX IX_Users_Login  ON Users (Login)`,
  `CREATE INDEX IX_Posts_CategoryId  ON Posts (CategoryId)`,
  `CREATE INDEX IX_Comments_PostId  ON Comments (PostId)`,
  `CREATE INDEX IX_Comments_UserId  ON Comments (UserId)`,
  `CREATE INDEX IX_PostTags_TagId  ON PostTags (TagId)`
];

export const dataQueries = [
  `INSERT INTO Categories (Name) VALUES ('Политика')`,
  `INSERT INTO Categories (Name) VALUES ('Спорт')`,
  `INSERT INTO Categories (Name) VALUES ('Политика')`,
  `INSERT INTO Categories (Name) VALUES ('Политика')`,

  `INSERT INTO Tags (Name) VALUES ('Новости')`,
  `INSERT INTO Tags (Name) VALUES ('2018')`,
  `INSERT INTO Tags (Name) VALUES ('Россия')`,
  `INSERT INTO Tags (Name) VALUES ('Деньги')`,
  `INSERT INTO Tags (Name) VALUES ('Доллар')`,

  `INSERT INTO Users (Confirmed, Email, LastName, Login, Name, Password, Role) VALUES (1, 'user3112@example.com', 'Denis', 'den79', 'Volkov', 'u32hvfbdhb', 'user')`,
  `INSERT INTO Users (Confirmed, Email, LastName, Login, Name, Password, Role) VALUES (1, 'user2223@example.com', 'Ivan', 'den232', 'Ivanov', 'u32hvfbdhb', 'user')`,
  `INSERT INTO Users (Confirmed, Email, LastName, Login, Name, Password, Role) VALUES (0, 'user12312@example.com', 'Nikolay', 'de232', 'Petrov', 'u32hvfbdhb', 'user')`,
  `INSERT INTO Users (Confirmed, Email, LastName, Login, Name, Password, Role) VALUES (1, 'user23233@example.com', 'Sergey', 'de2223239', 'Niloyaev', 'u32hvfbdhb', 'user')`,

  `INSERT INTO Posts (CategoryId, PublishTime, Title,  PostText) VALUES (1, '2017-01-27 10:00:00', 'Как строятся оптоволоконные сети', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum exercitationem dolores voluptas tempore iste in numquam inventore praesentium delectus facere? Dignissimos, expedita adipisci blanditiis officia ab accusamus voluptatem non dolores.')`,
  `INSERT INTO Posts (CategoryId, PublishTime, Title,  PostText) VALUES (2, '2018-03-13 09:00:00', 'Title 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum exercitationem dolores voluptas tempore iste in numquam inventore praesentium delectus facere? Dignissimos, expedita adipisci blanditiis officia ab accusamus voluptatem non dolores.')`,
  `INSERT INTO Posts (CategoryId, PublishTime, Title,  PostText) VALUES (2, '2018-04-17 17:00:00', 'Title 3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum exercitationem dolores voluptas tempore iste in numquam inventore praesentium delectus facere? Dignissimos, expedita adipisci blanditiis officia ab accusamus voluptatem non dolores.')`,
  `INSERT INTO Posts (CategoryId, PublishTime, Title,  PostText) VALUES (3, '2018-07-07 19:00:00', 'Title 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum exercitationem dolores voluptas tempore iste in numquam inventore praesentium delectus facere? Dignissimos, expedita adipisci blanditiis officia ab accusamus voluptatem non dolores.')`,

  `INSERT INTO Comments (PostId, PublishTime, Text, UserId) VALUES (1, '2017-01-27 10:00:00', 1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum exercitationem dolores voluptas tempore iste in numquam inventore praesentium delectus')`,
  `INSERT INTO Comments (PostId, PublishTime, Text, UserId) VALUES (2, '2017-01-27 10:00:00', 2, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum exercitationem dolores voluptas tempore iste in numquam inventore praesentium delectus')`,
  `INSERT INTO Comments (PostId, PublishTime, Text, UserId) VALUES (2, '2017-01-27 10:00:00', 2, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum exercitationem dolores voluptas tempore iste in numquam inventore praesentium delectus')`,
  `INSERT INTO Comments (PostId, PublishTime, Text, UserId) VALUES (4, '2017-01-27 10:00:00', 4, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum exercitationem dolores voluptas tempore iste in numquam inventore praesentium delectus')`,

  `INSERT INTO PostTags (PostId, TagId) VALUES (1, 1)`,
  `INSERT INTO PostTags (PostId, TagId) VALUES (1, 2)`,
  `INSERT INTO PostTags (PostId, TagId) VALUES (1, 3)`,
  `INSERT INTO PostTags (PostId, TagId) VALUES (2, 1)`,
  `INSERT INTO PostTags (PostId, TagId) VALUES (2, 2)`,
  `INSERT INTO PostTags (PostId, TagId) VALUES (3, 1)`,
  `INSERT INTO PostTags (PostId, TagId) VALUES (4, 1)`,
  `INSERT INTO PostTags (PostId, TagId) VALUES (4, 3)`,
  `INSERT INTO PostTags (PostId, TagId) VALUES (4, 4)`
];
