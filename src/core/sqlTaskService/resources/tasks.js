const tasks = [
  {
    id: 1,
    text: "Выбрать все статьи.",
    testQuery: "SELECT * FROM Posts"
  },
  {
    id: 2,
    text:
      'Выбрать все коментарии к статье с названием "Как строятся оптоволоконные сети" ',
    testQuery:
      "SELECT Comments.* FROM Comments INNER JOIN Posts ON Comments.PostId = Posts.Id WHERE Posts.Title = 'Как строятся оптоволоконные сети'"
  },
  {
    id: 3,
    text: 'Выбрать все статьи по категории "Спорт"',
    testQuery:
      "SELECT Posts.* FROM Posts INNER JOIN Categories ON Posts.CategoryId = Categories.Id WHERE Categories.Name = 'Спорт'"
  },
  {
    id: 4,
    text: "Выбрать все категории",
    testQuery: "SELECT * FROM Categories"
  },
  {
    id: 5,
    text: 'Выбрать все посты с тегом "Россия" ',
    testQuery:
      "SELECT Posts.* FROM Posts INNER JOIN PostTags ON Posts.Id = PostTags.PostId INNER JOIN Tags ON Tags.Id = PostTags.TagId WHERE Tags.Name = 'Россия'"
  }
];

export default tasks;
