import { createQueries, dataQueries } from './resources/testDbScript';

class SqlTaskService {
    constructor() {
        this.db = openDatabase(this.createGuid(), "0.1", this.createGuid(), 1024 * 1024 * 5);

        for (let i = 0; i < createQueries.length; i++) {
            this.processSql(createQueries[i], []);
        }

        for (let i = 0; i < dataQueries.length; i++) {
            this.processSql(dataQueries[i], []);
        }
    }

    processSql(query, params, callback, errorCallback) {
        this.db.transaction(
            function(tx) {
                tx.executeSql(
                    query,
                    params,
                    callback,
                    function(sqlError) {
                        errorCallback("Ошибка при выполнении запроса");
                    });
            },
            function(txError) {
                errorCallback("Ошибка при выполнении запроса");
            }
        );
    }

    testAnswer(userQuery, testQuery, callback) {
        this.processSql(
            userQuery, [],
            (tx, userResult) => {
                this.processSql(
                    testQuery, [],
                    (tx, testResult) => {
                        const errors = this.compareRows(userResult.rows, testResult.rows);
                        callback(!errors, errors);
                    },
                    errorText => callback(false, errorText));
            },
            (errorText => callback(false, errorText)));
    }

    compareRows(userRows, testRows) {
        const userKeys = Object.getOwnPropertyNames(userRows[0]);
        const testKeys = Object.getOwnPropertyNames(testRows[0]);

        if (userKeys.length != testKeys.length)
            return 'Количество столбцов в ответе не совпадает';

        if (userRows.length != testRows.length)
            return 'Количество строк в ответе не совпадает';

        for (let rowIndex = 0; rowIndex < testRows.length; rowIndex++) {
            for (let colIndex = 0; colIndex < testKeys.length; colIndex++) {
                const testValue = testRows[rowIndex][testKeys[rowIndex]];
                const userValue = testRows[rowIndex][testKeys[rowIndex]];
                if (userValue != testValue)
                    return 'Данные выборки не совпадают';
            }
        }
        return null;
    }


    createGuid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }
}

export default SqlTaskService;