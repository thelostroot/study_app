const q = `CREATE TABLE [__EFMigrationsHistory]
(
  MigrationId    NVARCHAR(150) NOT NULL
    CONSTRAINT PK___EFMigrationsHistory
    PRIMARY KEY,
  ProductVersion NVARCHAR(32)  NOT NULL
)
GO

CREATE TABLE Categories
(
  Id   INT IDENTITY
    CONSTRAINT PK_Categories
    PRIMARY KEY,
  Name NVARCHAR(MAX) NOT NULL
)
GO

CREATE TABLE Tags
(
  Id   INT IDENTITY
    CONSTRAINT PK_Tags
    PRIMARY KEY,
  Name NVARCHAR(MAX) NOT NULL
)
GO

CREATE TABLE Users
(
  Id        INT IDENTITY
    CONSTRAINT PK_Users
    PRIMARY KEY,
  Avatar    NVARCHAR(MAX),
  Confirmed BIT           NOT NULL,
  Email     NVARCHAR(450) NOT NULL,
  LastName  NVARCHAR(100) NOT NULL,
  Login     NVARCHAR(450) NOT NULL,
  Name      NVARCHAR(100) NOT NULL,
  Password  NVARCHAR(MAX) NOT NULL,
  Role      NVARCHAR(MAX)
)
GO

CREATE UNIQUE INDEX IX_Users_Email
  ON Users (Email)
GO

CREATE UNIQUE INDEX IX_Users_Login
  ON Users (Login)
GO

CREATE TABLE Posts
(
  Id          INT IDENTITY
    CONSTRAINT PK_Posts
    PRIMARY KEY,
  CategoryId  INT           NOT NULL
    CONSTRAINT FK_Posts_Categories_CategoryId
    REFERENCES Categories,
  Image       NVARCHAR(MAX),
  PostText    NVARCHAR(MAX) NOT NULL,
  PublishTime DATETIME2     NOT NULL,
  Title       NVARCHAR(MAX) NOT NULL
)
GO

CREATE INDEX IX_Posts_CategoryId
  ON Posts (CategoryId)
GO

CREATE TABLE Comments
(
  Id          INT IDENTITY
    CONSTRAINT PK_Comments
    PRIMARY KEY,
  PostId      INT           NOT NULL
    CONSTRAINT FK_Comments_Posts_PostId
    REFERENCES Posts,
  PublishTime DATETIME2     NOT NULL,
  Text        NVARCHAR(MAX) NOT NULL,
  UserId      INT           NOT NULL
    CONSTRAINT FK_Comments_Users_UserId
    REFERENCES Users
)
GO

CREATE INDEX IX_Comments_PostId
  ON Comments (PostId)
GO

CREATE INDEX IX_Comments_UserId
  ON Comments (UserId)
GO

CREATE TABLE PostTags
(
  PostId INT NOT NULL
    CONSTRAINT FK_PostTags_Posts_PostId
    REFERENCES Posts,
  TagId  INT NOT NULL
    CONSTRAINT FK_PostTags_Tags_TagId
    REFERENCES Tags,
  CONSTRAINT PK_PostTags
  PRIMARY KEY (PostId, TagId)
)
GO

CREATE INDEX IX_PostTags_TagId
  ON PostTags (TagId)
GO`;

